# Signalize API Bundle


## Installation
Add the repository to your composer file
```
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:signalize/apibundle.git"
        }
    ]
```

Add a require to your composer file
```
"signalize/apibundle": "dev-master"
```

Update composer
``` 
$ composer update
```

Add this bundle in your application kernel

```
// app/AppKernel.php
public function registerBundles()
{
    // ...
    Signalize\ApiBundle\SignalizeApiBundle::registerBundle($bundles);

    return $bundles;
}
```

Add the default configuration to your config.yml
```
// app/config/config.yml
imports:
...
    - { resource: "@SignalizeApiBundle/Resources/config/config.yml"}

```

Add the routing configuration to your routing.yml
```
// app/config/routing.yml

SignalizeApiBundle:
    resource: '@SignalizeApiBundle/Resources/config/routing.yml'
```


Add some parameters to your parameters.yml.dist
```
parameters:
    ...
    jwt_key_pass_phrase:    ~
    fos_user_email_address: ~
    fos_user_email_name:    ~
```

Generate the SSH keys :

``` bash
$ mkdir var/jwt
$ openssl genrsa -out var/jwt/private.pem -aes256 4096
$ openssl rsa -pubout -in var/jwt/private.pem -out var/jwt/public.pem
```
<?php

namespace Signalize\ApiBundle;

use FOS\UserBundle\FOSUserBundle;
use ApiPlatform\Core\Bridge\Symfony\Bundle\ApiPlatformBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SignalizeApiBundle extends Bundle
{
    static public function registerBundle(&$bundles)
    {
        $bundles[] = new self;
        $bundles[] = new ApiPlatformBundle();
//        $bundles[] = new FOSUserBundle();
    }

}

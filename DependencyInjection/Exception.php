<?php

namespace Signalize\ApiBundle\DependencyInjection;

use Signalize\ApiBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

class Exception extends BaseController
{

    public function showAction(Request $request, \Exception $exception, DebugLoggerInterface $logger = null)
    {
        $response = [
            'code' => $exception->getCode(),
            'message' => explode(PHP_EOL, trim($exception->getMessage(), PHP_EOL))
        ];

        if ($this->get('kernel')->isDebug()) {
            $response['debug_log'] = $logger->getLogs();
            $response['debug_trace'] = $exception->getTraceAsString();
        }


        return JsonResponse::create($response, ($exception->getCode() ?: 400));
    }

}
<?php

namespace Signalize\ApiBundle\DependencyInjection;

use Signalize\ApiBundle\Controller\BaseController;
use Signalize\ApiBundle\Entity\User;
use Signalize\ApiBundle\Form\LoginType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ValidateForm extends BaseController
{

    public function validate(Request $request, string $formType, string $entity = null): User
    {
        $form = $this->createForm($formType, (!empty($entity) ? new $entity : null));
        $form->handleRequest($request);
        $form->submit($request->request->all());
        if (!$form->isSubmitted() || !$form->isValid()) {
            throw new \Exception($form->getErrors(true, true), Response::HTTP_BAD_REQUEST);
        }
        return $form->getData();
    }
}



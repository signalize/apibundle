<?php

namespace Signalize\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\AttributeOverride;
use Doctrine\ORM\Mapping\AttributeOverrides;
use Doctrine\ORM\Mapping\Column;

/**
 * User.
 * @ORM\Table("fos_user")
 * @ORM\Entity
 * @AttributeOverrides({
 *      @AttributeOverride(name="usernameCanonical",
 *          column=@Column(
 *              type     = "string",
 *              length   = 155,
 *          )
 *      ),
 *      @AttributeOverride(name="emailCanonical",
 *          column=@Column(
 *              type     = "string",
 *              length   = 155,
 *          )
 *      )
 * })
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\ReadOnly()
     */
    protected $id;

    /**
     * @var
     * @Serializer\Expose()
     * @Assert\NotBlank(
     *     message="The username cannot be empty."
     * )
     * @Assert\Email(
     *     message="The username '{{ value }}' is not a valid email.",
     *     checkMX=true
     * )
     */
    protected $username;

    /**
     * @var
     * @Assert\Blank()
     * @Serializer\Groups({"hidden"})
     */
    protected $plainPassword;


    /**
     * @Serializer\Exclude()
     */
    protected $email;

    /**
     * @var
     * @Serializer\Expose()
     * @Serializer\Groups("read")
     */
    protected $enabled;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function __construct()
    {
        $this->created_at = new \DateTime();

        parent::__construct();
    }

    public function setEmail($email)
    {
        $email = is_null($email) ? '' : $email;
        parent::setEmail($email);
        $this->setUsername($email);

        return $this;
    }
}
